// Ratkaisu probleemaan, 6.6.2017, Mikko Karaiste

'use strict';

const returnSuitable = (x, y) => {

    const arr = [
        [0, 0, 0],
        [20, 20, 5],
        [10, 0, 12]
    ];

    const pluck = ((arr, property) => arr.map((station) => station[property]));

    let linkX = pluck(arr, 0);
    let linkY = pluck(arr, 1);
    let linkR = pluck(arr, 2);
    let objects = [];

    const returnDistances = ((arr) => {
        return arr.map((element, i) => {
            let a = element[0] - x;
            let b = element[1] - y;
            return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        });
    });

    let distances = returnDistances(arr);

    const returnPowers = ((arr) => {
        return arr.map((element, i) => {
            return Math.pow((element[2] - distances[i]), 2);
        })
    });

    let powers = returnPowers(arr);

    const createAndArrange = (() => {
        for (let i in arr) {
            if (distances[i] > arr[i][2]) {
                powers[i] = 0;
            } else {
                powers[i] = powers[i];
            }
        }

        for (let i in arr) {
            objects[i] = {
                devX: x,
                devY: y,
                linkX: linkX[i],
                linkY: linkY[i],
                distance: distances[i],
                power: powers[i],
            };
        }

        objects = objects.sort((a, b) => {
            return a.power < b.power;
        });

    });

    createAndArrange();

    const printMe = () => {
        if (objects[0]['power'] > 0) console.log(`Best link station for point ${x},${y} is ${objects[0]['linkX']},${objects[0]['linkY']} with power ${objects[0]['power'].toFixed(2)}`);
        else console.log(`No link station within reach for point ${x},${y}`);
    }

    printMe();
}

// (0,0), (100, 100), (15,10) and (18, 18).

console.log('\nResults\n');
returnSuitable(0, 0);
returnSuitable(100, 100);
returnSuitable(15, 10);
returnSuitable(18, 18);
console.log('\n');
